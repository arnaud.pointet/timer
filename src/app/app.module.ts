import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';

import { CustomNgbDateParserFormatter } from './formatter/ngbDateParserFormatter';

import { TaskService } from './services/task.service';

import { NavbarComponent } from './components/navbar/navbar.component';
import { TimerComponent } from './components/timer/timer.component';
import { TasksComponent } from './components/tasks/tasks.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TimerComponent,
    TasksComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule
  ],
  providers: [
    TaskService,
    { provide: NgbDateParserFormatter, useFactory: () => new CustomNgbDateParserFormatter('longDate')}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
