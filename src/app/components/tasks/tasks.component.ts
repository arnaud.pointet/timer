import { Component, OnInit } from '@angular/core'
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { TaskService } from '../../services/task.service'


@Component({
  selector: 'app-tasks',
  templateUrl: 'tasks.component.html'
})
export class TasksComponent implements OnInit {
  private tasks: any
  
  public constructor(
    private taskService: TaskService, 
    private ngbDateParserFormatter: NgbDateParserFormatter) {
      this.taskService.addNewTask.subscribe(task => {
        this.tasks.push(task)
      })
    }

  public ngOnInit() {
    this.tasks = this.taskService.getAllTasks()
  }

  private formatDate(date) {

    return this.ngbDateParserFormatter.format(date)
  }
}