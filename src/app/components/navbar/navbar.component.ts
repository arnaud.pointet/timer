import { Component } from '@angular/core';


@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html'
})
export class NavbarComponent {
  public isCollapsed: boolean = false;
  
  public constructor() {
    this.isCollapsed = (992 >= window.innerWidth)

    window.onresize = () => {
      this.isCollapsed = (992 >= window.innerWidth)
    }
  }

}
