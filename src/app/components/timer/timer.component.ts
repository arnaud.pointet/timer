import { Component } from '@angular/core'

import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { TaskService } from '../../services/task.service'


@Component({
  selector: 'app-timer',
  templateUrl: 'timer.component.html'
})
export class TimerComponent {
  public name: string
  public start_date: Date
  public start_date_string: Date
  public end_date: Date
  public end_date_string: Date

  public constructor (
    private taskService: TaskService,
    private ngbDateParserFormatter: NgbDateParserFormatter
  ) {}

  private onSelectDateChange(date: NgbDateStruct, target: string) {
    if (date != null) {
      this[target] = date;   //needed for first time around due to ngModel not binding during ngOnInit call. Seems like a bug in ng2.
      this[target + '_string'] = this.ngbDateParserFormatter.format(date);
    }
  }

  private addTask() {
    console.log(this.start_date)
    this.taskService.addTask({
      name: this.name,
      start_date: this.start_date,
      end_date: this.end_date
    })

    this.name = ''
    this.start_date = null
    this.end_date = null
  }
}