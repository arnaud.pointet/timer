import { Injectable, Output, EventEmitter} from '@angular/core';

@Injectable()

export class TaskService {
  @Output() addNewTask: EventEmitter<{}> = new EventEmitter

  public getAllTasks() {
    return [{
      name: 'tache1',
      start_date: {year: 2017, month: 6, day: 9},
      end_date: {year: 2017, month: 6, day: 21}
    },
    {
      name: 'tache2',
      start_date: {year: 2017, month: 6, day: 9},
      end_date: {year: 2017, month: 6, day: 25}
    }]
  }

  public addTask(task: any) {
    this.addNewTask.emit(task)
  }
}