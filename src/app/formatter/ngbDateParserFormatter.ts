import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap'

import { DatePipe } from '@angular/common'

export class CustomNgbDateParserFormatter extends NgbDateParserFormatter {
  private datePipe: DatePipe = new DatePipe('fr')

  public constructor(private dateFormatString: string) {
    super()
  }

  public format(date: NgbDateStruct): string {
    if (date === null) return ''

    try {
      return this.datePipe.transform(new Date(date.year, date.month - 1, date.day), this.dateFormatString)
    } catch (e) {
      console.log(e)
      return ''
    }
  }

  public parse(value: string): NgbDateStruct {
    let returnValue: NgbDateStruct
    
    if (!value) {
			returnValue = null;
		} else {
			try {
				let dateParts = this.datePipe.transform(value, 'M-d-y').split('-');
				returnValue = { year: parseInt(dateParts[2]), month: parseInt(dateParts[0]), day: parseInt(dateParts[1]) };
			} catch (e) {
				returnValue = null;
			}
		}
    return returnValue
  }
}